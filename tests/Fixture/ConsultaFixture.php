<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConsultaFixture
 */
class ConsultaFixture extends TestFixture
{
    /**
     * Table name
     *
     * @var string
     */
    public $table = 'consulta';
    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'fecha' => ['type' => 'date', 'length' => null, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null],
        'medico_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'paciente_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'diagnostico_id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'tratamiento' => ['type' => 'string', 'length' => 250, 'null' => false, 'default' => null, 'collate' => 'utf8_spanish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'costo' => ['type' => 'decimal', 'length' => 10, 'precision' => 2, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => ''],
        '_indexes' => [
            'fk_consulta_medico_idx' => ['type' => 'index', 'columns' => ['medico_id'], 'length' => []],
            'fk_paciente_idx' => ['type' => 'index', 'columns' => ['paciente_id'], 'length' => []],
            'fk_consulta_diagnostico_idx' => ['type' => 'index', 'columns' => ['diagnostico_id'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'id_UNIQUE' => ['type' => 'unique', 'columns' => ['id'], 'length' => []],
            'fk_consulta_diagnostico' => ['type' => 'foreign', 'columns' => ['diagnostico_id'], 'references' => ['diagnosticos', 'id'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_consulta_medico' => ['type' => 'foreign', 'columns' => ['medico_id'], 'references' => ['medicos', 'id_medico'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
            'fk_consulta_paciente' => ['type' => 'foreign', 'columns' => ['paciente_id'], 'references' => ['pacientes', 'id_paciente'], 'update' => 'noAction', 'delete' => 'noAction', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'utf8_spanish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd
    /**
     * Init method
     *
     * @return void
     */
    public function init()
    {
        $this->records = [
            [
                'id' => 1,
                'fecha' => '2021-04-13',
                'medico_id' => 1,
                'paciente_id' => 1,
                'diagnostico_id' => 1,
                'tratamiento' => 'Lorem ipsum dolor sit amet',
                'costo' => 1.5,
            ],
        ];
        parent::init();
    }
}
