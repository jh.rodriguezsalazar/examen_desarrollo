<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PerfilDetalleTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\PerfilDetalleTable Test Case
 */
class PerfilDetalleTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\PerfilDetalleTable
     */
    public $PerfilDetalle;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.PerfilDetalle',
        'app.Perfil',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('PerfilDetalle') ? [] : ['className' => PerfilDetalleTable::class];
        $this->PerfilDetalle = TableRegistry::getTableLocator()->get('PerfilDetalle', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->PerfilDetalle);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
