<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ReporteTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ReporteTable Test Case
 */
class ReporteTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\ReporteTable
     */
    public $Reporte;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Reporte',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Reporte') ? [] : ['className' => ReporteTable::class];
        $this->Reporte = TableRegistry::getTableLocator()->get('Reporte', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Reporte);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
