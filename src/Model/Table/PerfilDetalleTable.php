<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PerfilDetalle Model
 *
 * @property \App\Model\Table\PerfilTable&\Cake\ORM\Association\BelongsTo $Perfil
 *
 * @method \App\Model\Entity\PerfilDetalle get($primaryKey, $options = [])
 * @method \App\Model\Entity\PerfilDetalle newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PerfilDetalle[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PerfilDetalle|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PerfilDetalle saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PerfilDetalle patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PerfilDetalle[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PerfilDetalle findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PerfilDetalleTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('perfil_detalle');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Perfil', [
            'foreignKey' => 'perfil_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->scalar('descripcion')
            ->maxLength('descripcion', 255)
            ->allowEmptyString('descripcion');

        $validator
            ->boolean('crear')
            ->requirePresence('crear', 'create')
            ->notEmptyString('crear');

        $validator
            ->boolean('editar')
            ->allowEmptyString('editar');

        $validator
            ->boolean('borrado_logico')
            ->allowEmptyString('borrado_logico');

        $validator
            ->boolean('ver')
            ->allowEmptyString('ver');

        $validator
            ->boolean('borrado')
            ->notEmptyString('borrado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['perfil_id'], 'Perfil'));

        return $rules;
    }
}
