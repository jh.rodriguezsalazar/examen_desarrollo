<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Consulta Model
 *
 * @property \App\Model\Table\MedicosTable&\Cake\ORM\Association\BelongsTo $Medicos
 * @property \App\Model\Table\PacientesTable&\Cake\ORM\Association\BelongsTo $Pacientes
 * @property \App\Model\Table\DiagnosticosTable&\Cake\ORM\Association\BelongsTo $Diagnosticos
 *
 * @method \App\Model\Entity\Consultum get($primaryKey, $options = [])
 * @method \App\Model\Entity\Consultum newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Consultum[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Consultum|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consultum saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consultum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Consultum[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Consultum findOrCreate($search, callable $callback = null, $options = [])
 */
class ConsultaTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('consulta');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Medicos', [
            'foreignKey' => 'medico_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Pacientes', [
            'foreignKey' => 'paciente_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Diagnosticos', [
            'foreignKey' => 'diagnostico_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->date('fecha')
            ->requirePresence('fecha', 'create')
            ->notEmptyDate('fecha');

        $validator
            ->scalar('tratamiento')
            ->maxLength('tratamiento', 250)
            ->requirePresence('tratamiento', 'create')
            ->notEmptyString('tratamiento');

        $validator
            ->decimal('costo')
            ->requirePresence('costo', 'create')
            ->notEmptyString('costo');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));
        $rules->add($rules->existsIn(['medico_id'], 'Medicos'));
        $rules->add($rules->existsIn(['paciente_id'], 'Pacientes'));
        $rules->add($rules->existsIn(['diagnostico_id'], 'Diagnosticos'));

        return $rules;
    }
}
