<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * PerfilDetalle Entity
 *
 * @property int $id
 * @property int $perfil_id
 * @property string|null $descripcion
 * @property bool $crear
 * @property bool|null $editar
 * @property bool|null $borrado_logico
 * @property bool|null $ver
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property bool $borrado
 *
 * @property \App\Model\Entity\Perfil $perfil
 */
class PerfilDetalle extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'perfil_id' => true,
        'descripcion' => true,
        'crear' => true,
        'editar' => true,
        'borrado_logico' => true,
        'ver' => true,
        'created' => true,
        'modified' => true,
        'borrado' => true,
        'perfil' => true,
    ];
}
