<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Consultum Entity
 *
 * @property int $id
 * @property \Cake\I18n\FrozenDate $fecha
 * @property int $medico_id
 * @property int $paciente_id
 * @property int $diagnostico_id
 * @property string $tratamiento
 * @property float $costo
 *
 * @property \App\Model\Entity\Medico $medico
 * @property \App\Model\Entity\Paciente $paciente
 * @property \App\Model\Entity\Diagnostico $diagnostico
 */
class Consultum extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'fecha' => true,
        'medico_id' => true,
        'paciente_id' => true,
        'diagnostico_id' => true,
        'tratamiento' => true,
        'costo' => true,
        'medico' => true,
        'paciente' => true,
        'diagnostico' => true,
    ];
}
