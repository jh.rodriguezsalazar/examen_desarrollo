<div class="consulta index large-12 medium-12 columns content">
    <div class="large-10 medium-10 columns">
        <h3><?= __('Reporte') ?></h3>
    </div>
    <div class="large-2 medium-2 columns" style="align-content: right;">
        <?= $this->Html->link('Descargar Reporte', ['controller'=>'Reporte','action'=>'exportar']);?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Fecha') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Consulta') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Total') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            $fecha = '';
            foreach ($reporte as $index){ 
                if ($index->fecha != $fecha)
                {
                    $fecha = $index->fecha;
                    echo '<tr>';
                    echo '<td>'.$index->fecha. '</td>';
                    echo '<td> </td>';
                    echo '<td> </td>';
                    echo '<tr>';
                }
            ?>
            <tr>
                <td><?= h($index->medico) ?></td>
                <td><?= h($index->consultas) ?></td>
                <td><?= h($index->costo) ?></td>
            </tr>
            <?php } ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiete') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, Mostrando {{current}} dato(s) de {{count}}')]) ?></p>
    </div>
</div>
