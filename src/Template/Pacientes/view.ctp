<div class="pacientes view large-9 medium-8 columns content">
    <h3><?= h($paciente->id_paciente) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($paciente->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Paciente') ?></th>
            <td><?= $this->Number->format($paciente->id_paciente) ?></td>
        </tr>
    </table>
</div>
