<div class="usuario view large-9 medium-8 columns content">
    <h3><?= h($usuario->nombre) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Usuario') ?></th>
            <td><?= h($usuario->user) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contraseña') ?></th>
            <td><?= h($usuario->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Perfil') ?></th>
            <td><?= $usuario->has('perfil') ? $this->Html->link($usuario->perfil->descripcion, ['controller' => 'Perfil', 'action' => 'view', $usuario->perfil->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Creado') ?></th>
            <td><?= h($usuario->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modificado') ?></th>
            <td><?= h($usuario->modified) ?></td>
        </tr>
    </table>
</div>
