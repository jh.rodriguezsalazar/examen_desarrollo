<div class="usuario form large-9 medium-8 columns content">
    <?= $this->Form->create($usuario) ?>
    <fieldset>
        <legend><?= __('Add Usuario') ?></legend>
        <?php
            echo $this->Form->control('nombre',['label'=>'Nombre Completo']);
            echo $this->Form->control('user',['label'=>'Usuario']);
            echo $this->Form->control('password',['type'=>'password', 'label'=>'Contraseña']);
            echo $this->Form->control('perfil_id', ['options' => $perfil, 'label'=>'Perfil']);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
