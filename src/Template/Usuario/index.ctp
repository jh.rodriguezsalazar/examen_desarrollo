<div class="usuario index large-11 medium-10 columns content">
    <div class="row">
        <div class="large-10 medium-10 columns">
            <h3><?= __('Usuario') ?></h3>
        </div>
        <div class="large-2 medium-2 columns" style="align-content: right;">
            <?= $this->Html->link('Agregar Usuario', ['controller'=>'Usuario','action'=>'add']);?>
        </div>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Usuario') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Contraseña') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Perfil') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Creado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Modificado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($usuario as $usuario): ?>
            <tr>
                <td><?= h($usuario->nombre) ?></td>
                <td><?= h($usuario->user) ?></td>
                <td><?= h($usuario->password) ?></td>
                <td><?= $usuario->has('perfil') ? $this->Html->link($usuario->perfil->id, ['controller' => 'Perfil', 'action' => 'view', $usuario->perfil->id]) : '' ?></td>
                <td><?= h($usuario->created) ?></td>
                <td><?= h($usuario->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $usuario->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $usuario->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $usuario->id], ['confirm' => __('Esta seguro que desea borrar este registro? # {0}?', $usuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, Mostrando {{current}} resultado(s) de {{count}}')]) ?></p>
    </div>
</div>
