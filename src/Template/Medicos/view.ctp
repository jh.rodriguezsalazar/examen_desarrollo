<div class="medicos view large-9 medium-8 columns content">
    <h3><?= h($medico->id_medico) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($medico->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id Medico') ?></th>
            <td><?= $this->Number->format($medico->id_medico) ?></td>
        </tr>
    </table>
</div>
