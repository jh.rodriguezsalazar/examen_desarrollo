<div class="perfil view large-11 medium-10 columns content">
    <h3><?= h($perfil->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descripcion') ?></th>
            <td><?= h($perfil->descripcion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($perfil->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($perfil->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($perfil->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Borrado') ?></th>
            <td><?= $perfil->borrado ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Perfil Detalle') ?></h4>
        <?php if (!empty($perfil->perfil_detalle)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Perfil Id') ?></th>
                <th scope="col"><?= __('Descripcion') ?></th>
                <th scope="col"><?= __('Crear') ?></th>
                <th scope="col"><?= __('Editar') ?></th>
                <th scope="col"><?= __('Borrado Logico') ?></th>
                <th scope="col"><?= __('Ver') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Borrado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($perfil->perfil_detalle as $perfilDetalle): ?>
            <tr>
                <td><?= h($perfilDetalle->id) ?></td>
                <td><?= h($perfilDetalle->perfil_id) ?></td>
                <td><?= h($perfilDetalle->descripcion) ?></td>
                <td><?= h($perfilDetalle->crear) ?></td>
                <td><?= h($perfilDetalle->editar) ?></td>
                <td><?= h($perfilDetalle->borrado_logico) ?></td>
                <td><?= h($perfilDetalle->ver) ?></td>
                <td><?= h($perfilDetalle->created) ?></td>
                <td><?= h($perfilDetalle->modified) ?></td>
                <td><?= h($perfilDetalle->borrado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'PerfilDetalle', 'action' => 'view', $perfilDetalle->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'PerfilDetalle', 'action' => 'edit', $perfilDetalle->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'PerfilDetalle', 'action' => 'delete', $perfilDetalle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $perfilDetalle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Usuario') ?></h4>
        <?php if (!empty($perfil->usuario)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User') ?></th>
                <th scope="col"><?= __('Password') ?></th>
                <th scope="col"><?= __('Perfil Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col"><?= __('Borrado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($perfil->usuario as $usuario): ?>
            <tr>
                <td><?= h($usuario->id) ?></td>
                <td><?= h($usuario->user) ?></td>
                <td><?= h($usuario->password) ?></td>
                <td><?= h($usuario->perfil_id) ?></td>
                <td><?= h($usuario->created) ?></td>
                <td><?= h($usuario->modified) ?></td>
                <td><?= h($usuario->borrado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Usuario', 'action' => 'view', $usuario->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Usuario', 'action' => 'edit', $usuario->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Usuario', 'action' => 'delete', $usuario->id], ['confirm' => __('Are you sure you want to delete # {0}?', $usuario->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
