<div class="perfil form large-11 medium-10 columns content">
    <?= $this->Form->create($perfil) ?>
    <fieldset>
        <legend><?= __('Edit Perfil') ?></legend>
        <?php
            echo $this->Form->control('descripcion');
            echo $this->Form->control('borrado');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
