
<div class="diagnosticos view large-9 medium-8 columns content">
    <h3><?= h($diagnostico->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Descripcion') ?></th>
            <td><?= h($diagnostico->descripcion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($diagnostico->id) ?></td>
        </tr>
    </table>
</div>
