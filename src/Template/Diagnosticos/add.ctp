
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Diagnosticos'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="diagnosticos form large-9 medium-8 columns content">
    <?= $this->Form->create($diagnostico) ?>
    <fieldset>
        <legend><?= __('Add Diagnostico') ?></legend>
        <?php
            echo $this->Form->control('descripcion');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
