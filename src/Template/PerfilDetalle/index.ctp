<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PerfilDetalle[]|\Cake\Collection\CollectionInterface $perfilDetalle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Perfil Detalle'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Perfil'), ['controller' => 'Perfil', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Perfil'), ['controller' => 'Perfil', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="perfilDetalle index large-9 medium-8 columns content">
    <h3><?= __('Perfil Detalle') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('perfil_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('descripcion') ?></th>
                <th scope="col"><?= $this->Paginator->sort('crear') ?></th>
                <th scope="col"><?= $this->Paginator->sort('editar') ?></th>
                <th scope="col"><?= $this->Paginator->sort('borrado_logico') ?></th>
                <th scope="col"><?= $this->Paginator->sort('ver') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col"><?= $this->Paginator->sort('borrado') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($perfilDetalle as $perfilDetalle): ?>
            <tr>
                <td><?= $this->Number->format($perfilDetalle->id) ?></td>
                <td><?= $perfilDetalle->has('perfil') ? $this->Html->link($perfilDetalle->perfil->id, ['controller' => 'Perfil', 'action' => 'view', $perfilDetalle->perfil->id]) : '' ?></td>
                <td><?= h($perfilDetalle->descripcion) ?></td>
                <td><?= h($perfilDetalle->crear) ?></td>
                <td><?= h($perfilDetalle->editar) ?></td>
                <td><?= h($perfilDetalle->borrado_logico) ?></td>
                <td><?= h($perfilDetalle->ver) ?></td>
                <td><?= h($perfilDetalle->created) ?></td>
                <td><?= h($perfilDetalle->modified) ?></td>
                <td><?= h($perfilDetalle->borrado) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $perfilDetalle->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $perfilDetalle->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $perfilDetalle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $perfilDetalle->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
