<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PerfilDetalle $perfilDetalle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Perfil Detalle'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Perfil'), ['controller' => 'Perfil', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Perfil'), ['controller' => 'Perfil', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="perfilDetalle form large-9 medium-8 columns content">
    <?= $this->Form->create($perfilDetalle) ?>
    <fieldset>
        <legend><?= __('Add Perfil Detalle') ?></legend>
        <?php
            echo $this->Form->control('perfil_id', ['options' => $perfil]);
            echo $this->Form->control('descripcion');
            echo $this->Form->control('crear');
            echo $this->Form->control('editar');
            echo $this->Form->control('borrado_logico');
            echo $this->Form->control('ver');
            echo $this->Form->control('borrado');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
