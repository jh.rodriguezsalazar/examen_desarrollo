<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PerfilDetalle $perfilDetalle
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Perfil Detalle'), ['action' => 'edit', $perfilDetalle->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Perfil Detalle'), ['action' => 'delete', $perfilDetalle->id], ['confirm' => __('Are you sure you want to delete # {0}?', $perfilDetalle->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Perfil Detalle'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Perfil Detalle'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Perfil'), ['controller' => 'Perfil', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Perfil'), ['controller' => 'Perfil', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="perfilDetalle view large-9 medium-8 columns content">
    <h3><?= h($perfilDetalle->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Perfil') ?></th>
            <td><?= $perfilDetalle->has('perfil') ? $this->Html->link($perfilDetalle->perfil->id, ['controller' => 'Perfil', 'action' => 'view', $perfilDetalle->perfil->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Descripcion') ?></th>
            <td><?= h($perfilDetalle->descripcion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($perfilDetalle->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($perfilDetalle->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($perfilDetalle->modified) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Crear') ?></th>
            <td><?= $perfilDetalle->crear ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Editar') ?></th>
            <td><?= $perfilDetalle->editar ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Borrado Logico') ?></th>
            <td><?= $perfilDetalle->borrado_logico ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Ver') ?></th>
            <td><?= $perfilDetalle->ver ? __('Yes') : __('No'); ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Borrado') ?></th>
            <td><?= $perfilDetalle->borrado ? __('Yes') : __('No'); ?></td>
        </tr>
    </table>
</div>
