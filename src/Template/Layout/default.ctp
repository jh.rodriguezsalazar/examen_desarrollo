<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?>:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('base.css') ?>
    <?= $this->Html->css('style.css') ?>
    <!-- <?= $this->Html->css('all.css') ?> -->      <!-- Font Awesome -->
    <!-- <?= $this->Html->css('bootstrap.min.css') ?>  -->     <!-- Bootstrap -->  
    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <?=$this->Html->css('all.min')?>
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">

    <?= $this->Html->script('jquery-3.6.0.min.js') ?>      <!-- JQuery -->

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <nav class="top-bar expanded" data-topbar role="navigation">
        <ul class="title-area large-2 medium-3 columns">
            <li class="name">
                <h1><a href=""><?= __('Gobierno del Estado') ?></a></h1>
                <!-- <?= $this->Form->button(__('boton'));?> -->
            </li>
        </ul>
        <div class="top-bar-section">
            <ul class="right">
                <!-- <li><a target="_blank" href="https://book.cakephp.org/3/">Documentation</a></li>
                <li><a target="_blank" href="https://api.cakephp.org/3.0/">API</a></li> -->
            </ul>
        </div>
    </nav>
    <nav class="large-2 medium-3 columns" id="actions-sidebar">
        <!-- <ul class="side-nav flex-column"> -->
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        
            <li class="nav-item">
              <a href="" class="nav-link">
               <!--  <i class="nav-icon fas fa-cogs"></i> -->
                <p> Menú </p>
              </a>

              <ul class="nav nav-treeview">
                <li>
                  <?=$this->Html->link('<p> Consulta </p>', ['controller' => 'Consulta', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link'])?>
                </li>
              </ul>
             <ul class="nav nav-treeview">
                <li>
                  <?=$this->Html->link('<p> Reporte </p>', ['controller' => 'Reporte', 'action' => 'index'], ['escape' => false, 'class' => 'nav-link'])?>
                </li>
              </ul>

            </li>

        </ul>
    </nav>
    <?= $this->Flash->render() ?>
    <div class="container clearfix">
        <?= $this->fetch('content') ?>
    </div>
    <footer>
    </footer>
</body>
</html>
