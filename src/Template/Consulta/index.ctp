<div class="consulta index large-12 medium-12 columns content">
    <div class="large-10 medium-10 columns">
        <h3><?= __('Consulta') ?></h3>
    </div>
    <div class="large-2 medium-2 columns" style="align-content: right;">
        <?= $this->Html->link('Agregar Consulta', ['controller'=>'Consulta','action'=>'add']);?>
    </div>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col" width='2%'></th>
                <th scope="col" width='5%'><?= $this->Paginator->sort('id') ?></th>
                <th scope="col" width='8%'><?= $this->Paginator->sort('fecha') ?></th>
                <th scope="col"><?= $this->Paginator->sort('medico_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('paciente_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('diagnostico_id') ?></th>
                <th scope="col" width='10%'><?= $this->Paginator->sort('tratamiento') ?></th>
                <th scope="col" width='5%'><?= $this->Paginator->sort('costo') ?></th>
                <th scope="col" class="actions"><?= __('Actiones') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($consulta as $consultum): ?>
            <tr>
                <td><i class="bi bi-plus-square-fill"></i></td>
                <td><?= $this->Number->format($consultum->id) ?></td>
                <td><?= h($consultum->fecha) ?></td>
                <td><?= $consultum->has('medico') ? $this->Html->link($consultum->medico->nombre, ['controller' => 'Medicos', 'action' => 'view', $consultum->medico->id_medico]) : '' ?></td>
                <td><?= $consultum->has('paciente') ? $this->Html->link($consultum->paciente->nombre, ['controller' => 'Pacientes', 'action' => 'view', $consultum->paciente->id_paciente]) : '' ?></td>
                <td><?= $consultum->has('diagnostico') ? $this->Html->link($consultum->diagnostico->descripcion, ['controller' => 'Diagnosticos', 'action' => 'view', $consultum->diagnostico->id]) : '' ?></td>
                <td><?= h($consultum->tratamiento) ?></td>
                <td><?= $this->Number->format($consultum->costo) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $consultum->id]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $consultum->id]) ?>
                    <?= $this->Form->postLink(__('Borrar'), ['action' => 'delete', $consultum->id], ['confirm' => __('Are you sure you want to delete # {0}?', $consultum->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiete') . ' >') ?>
            <?= $this->Paginator->last(__('Último') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, Mostrando {{current}} dato(s) de {{count}}')]) ?></p>
    </div>
</div>


<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css">
