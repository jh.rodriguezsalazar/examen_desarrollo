<div class="consulta view large-9 medium-8 columns content">
    <h3><?= h('Consulta') ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Medico') ?></th>
            <td><?= $consultum->has('medico') ? $this->Html->link($consultum->medico->nombre, ['controller' => 'Medicos', 'action' => 'view', $consultum->medico->id_medico]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Paciente') ?></th>
            <td><?= $consultum->has('paciente') ? $this->Html->link($consultum->paciente->nombre, ['controller' => 'Pacientes', 'action' => 'view', $consultum->paciente->id_paciente]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Diagnostico') ?></th>
            <td><?= $consultum->has('diagnostico') ? $this->Html->link($consultum->diagnostico->descripcion, ['controller' => 'Diagnosticos', 'action' => 'view', $consultum->diagnostico->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Tratamiento') ?></th>
            <td><?= h($consultum->tratamiento) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($consultum->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Costo') ?></th>
            <td><?= $this->Number->format($consultum->costo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($consultum->fecha) ?></td>
        </tr>
    </table>
</div>
