<div class="consulta form large-9 medium-8 columns content">
    <?= $this->Form->create($consultum) ?>
    <fieldset>
        <legend><?= __('Agregar Consulta') ?></legend>
        <?php
            echo $this->Form->control('fecha',['type'=>'date']);
            echo $this->Form->control('medico_id', ['options' => $medicos]);
            echo $this->Form->control('paciente_id', ['options' => $pacientes]);
            echo $this->Form->control('diagnostico_id', ['options' => $diagnosticos]);
            echo $this->Form->control('tratamiento');
            echo $this->Form->control('costo');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Guardar')) ?>
    <?= $this->Form->end() ?>
</div>
