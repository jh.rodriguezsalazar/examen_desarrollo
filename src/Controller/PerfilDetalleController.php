<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * PerfilDetalle Controller
 *
 * @property \App\Model\Table\PerfilDetalleTable $PerfilDetalle
 *
 * @method \App\Model\Entity\PerfilDetalle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PerfilDetalleController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Perfil'],
        ];
        $perfilDetalle = $this->paginate($this->PerfilDetalle);

        $this->set(compact('perfilDetalle'));
    }

    /**
     * View method
     *
     * @param string|null $id Perfil Detalle id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $perfilDetalle = $this->PerfilDetalle->get($id, [
            'contain' => ['Perfil'],
        ]);

        $this->set('perfilDetalle', $perfilDetalle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $perfilDetalle = $this->PerfilDetalle->newEntity();
        if ($this->request->is('post')) {
            $perfilDetalle = $this->PerfilDetalle->patchEntity($perfilDetalle, $this->request->getData());
            if ($this->PerfilDetalle->save($perfilDetalle)) {
                $this->Flash->success(__('The perfil detalle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The perfil detalle could not be saved. Please, try again.'));
        }
        $perfil = $this->PerfilDetalle->Perfil->find('list', ['limit' => 200]);
        $this->set(compact('perfilDetalle', 'perfil'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Perfil Detalle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $perfilDetalle = $this->PerfilDetalle->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $perfilDetalle = $this->PerfilDetalle->patchEntity($perfilDetalle, $this->request->getData());
            if ($this->PerfilDetalle->save($perfilDetalle)) {
                $this->Flash->success(__('The perfil detalle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The perfil detalle could not be saved. Please, try again.'));
        }
        $perfil = $this->PerfilDetalle->Perfil->find('list', ['limit' => 200]);
        $this->set(compact('perfilDetalle', 'perfil'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Perfil Detalle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $perfilDetalle = $this->PerfilDetalle->get($id);
        if ($this->PerfilDetalle->delete($perfilDetalle)) {
            $this->Flash->success(__('The perfil detalle has been deleted.'));
        } else {
            $this->Flash->error(__('The perfil detalle could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
