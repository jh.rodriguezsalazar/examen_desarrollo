<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Security;

/**
 * Usuario Controller
 *
 * @property \App\Model\Table\UsuarioTable $Usuario
 *
 * @method \App\Model\Entity\Usuario[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsuarioController extends AppController
{
    /**
     * initialize
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('UserComponent');
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Perfil'],
        ];
        $usuario = $this->paginate($this->Usuario);
        foreach ($usuario->toArray() as $key ) {
            $key->id = $this->UserComponent->encrypt($key->id);
        }

        $this->set(compact('usuario'));
    }

    /**
     * View method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        if (!is_null($id)) $id = $this->UserComponent->decrypt($id);
        $usuario = $this->Usuario->get($id, [
            'contain' => ['Perfil'],
        ]);
        
        $this->set(compact('usuario','perfil'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $usuario = $this->Usuario->newEntity();
        if ($this->request->is('post')) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            if ($this->Usuario->save($usuario)) {
                $this->Flash->success(__('El usuario fue registrado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo ser guardado, intentelo de nuevo por favor.'));
        }
        $perfil = $this->Usuario->Perfil->find('list', ['keyField' => 'id','valueField' => 'descripcion','limit' => 200]);

        $this->set(compact('usuario', 'perfil'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        if (!is_null($id)) $id = $this->UserComponent->decrypt($id);
        $usuario = $this->Usuario->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $usuario = $this->Usuario->patchEntity($usuario, $this->request->getData());
            if ($this->Usuario->save($usuario)) {
                $this->Flash->success(__('El usuario fue actualizad.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no pudo ser guardado, intente de nuevo por favor.'));
        }
        $perfil = $this->Usuario->Perfil->find('list', ['keyField' => 'id','valueField' => 'descripcion','limit' => 200]);
        $this->set(compact('usuario', 'perfil'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Usuario id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        if (!is_null($id)) $id = $this->UserComponent->decrypt($id);
        $this->request->allowMethod(['post', 'delete']);
        $usuario = $this->Usuario->get($id);
        if ($this->Usuario->delete($usuario)) {
            $this->Flash->success(__('El usuario fue borado corectamente.'));
        } else {
            $this->Flash->error(__('El usuario no pudo ser borrado, por favor intentelo de nuevo'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
