<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Consulta Controller
 *
 * @property \App\Model\Table\ConsultaTable $Consulta
 *
 * @method \App\Model\Entity\Consultum[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConsultaController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Medicos', 'Pacientes', 'Diagnosticos'],
        ];
        $consulta = $this->paginate($this->Consulta);

        foreach ($consulta as $key ) {
            $key->costo *= 1.16;
        }

        $this->set(compact('consulta'));
    }

    /**
     * View method
     *
     * @param string|null $id Consultum id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $consultum = $this->Consulta->get($id, [
            'contain' => ['Medicos', 'Pacientes', 'Diagnosticos'],
        ]);

        $this->set('consultum', $consultum);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $consultum = $this->Consulta->newEntity();
        if ($this->request->is('post')) {
            $consultum = $this->Consulta->patchEntity($consultum, $this->request->getData());
            if ($this->Consulta->save($consultum)) {
                $this->Flash->success(__('La consulta fue registrada correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La consulta no fue registrada, intente de nuevo por favor'));
        }
        $medicos = $this->Consulta->Medicos->find('list', ['keyField' => 'id','valueField' => 'nombre','limit' => 200]);
        $pacientes = $this->Consulta->Pacientes->find('list', ['keyField' => 'id','valueField' => 'nombre','limit' => 200]);
        $diagnosticos = $this->Consulta->Diagnosticos->find('list', ['keyField' => 'id','valueField' => 'descripcion','limit' => 200]);
        $this->set(compact('consultum', 'medicos', 'pacientes', 'diagnosticos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Consultum id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $consultum = $this->Consulta->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $consultum = $this->Consulta->patchEntity($consultum, $this->request->getData());
            if ($this->Consulta->save($consultum)) {
                $this->Flash->success(__('La consulta fue actualizada correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La consulta no fue actualizada, intente de nuevo por favor'));
        }
        $medicos = $this->Consulta->Medicos->find('list', ['keyField' => 'id','valueField' => 'nombre','limit' => 200]);
        $pacientes = $this->Consulta->Pacientes->find('list', ['keyField' => 'id','valueField' => 'nombre','limit' => 200]);
        $diagnosticos = $this->Consulta->Diagnosticos->find('list', ['keyField' => 'id','valueField' => 'descripcion','limit' => 200]);
        $this->set(compact('consultum', 'medicos', 'pacientes', 'diagnosticos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Consultum id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $consultum = $this->Consulta->get($id);
        if ($this->Consulta->delete($consultum)) {
            $this->Flash->success(__('La consulta fue eliminada correctamente.'));
        } else {
            $this->Flash->error(__('La consulta no fue eliminada, intente de nuevo por favor'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
