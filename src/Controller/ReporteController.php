<?php
namespace App\Controller;

use App\Controller\AppController;
use Box\Spout\Writer\Common\Creator\WriterEntityFactory;
use Box\Spout\Common\Entity\Row;


/**
 * Reporte Controller
 *
 * @property \App\Model\Table\ReporteTable $Reporte
 *
 * @method \App\Model\Entity\Reporte[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ReporteController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $reporte = $this->paginate($this->Reporte);
        $this->set(compact('reporte'));
    }

    public function exportar()
    {
        $writer = WriterEntityFactory::createXLSXWriter();
        $writer->openToBrowser('Reporte');
        $reporte = $this->paginate($this->Reporte)->toArray();
        
        //Encabezado
        $writer->addRow(WriterEntityFactory::createRowFromArray(['Dias','Consultas','Total']));  

        //Información del reporte
        $fecha = '';
        foreach ($reporte as $key ) {
            if( $key->fecha != $fecha )
            {
                $fecha = $key->fecha;
                $writer->addRow(WriterEntityFactory::createRowFromArray([$fecha])); 
            }
            $writer->addRow(WriterEntityFactory::createRowFromArray([$key->medico, $key->consultas, $key->costo])); 
        }

        $writer->openToBrowser('Reporte.xlsx'); // stream data directly to the browser 
        $writer->close();
       
    }
    
}
