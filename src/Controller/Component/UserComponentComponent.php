<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

/**
 * UserComponent component
 */
class UserComponentComponent extends Component
{
    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];
    
    public function encrypt($value)
    {
        return base64_encode($value);
    }
    public function decrypt($value)
    {
        return base64_decode($value);
    }


}
